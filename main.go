package main

import (
	// remember to add encoding/xml to your list of imports
	"encoding/base64"
	"encoding/xml"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

var severityMap = map[string]int{
	"Information": 0,
	"Low":         1,
	"Medium":      2,
	"High":        3,
}

const template = `Severity: %s

Confidence: %s

Host: %s

Path: %s


Issue Detail
%s
Issue Background
$s

Issue Remediation
%s

Evidence
Request:
%s
Response:
%s

References
%s


Vulnerability Classifications
%s`

type Issues struct {
	XMLName xml.Name `xml:"issues"`
	Issue   []struct {
		Name string `xml:"name"`
		Type int64  `xml:"type"`
		Host struct {
			Text string `xml:",chardata"`
			Ip   string `xml:"ip,attr"`
		} `xml:"host"`
		Path                         string `xml:"path"`
		Location                     string `xml:"location"`
		Severity                     string `xml:"severity"`
		Confidence                   string `xml:"confidence"`
		IssueBackground              string `xml:"issueBackground"`
		RemediationBackground        string `xml:"remediationBackground"`
		References                   string `xml:"references"`
		VulnerabilityClassifications string `xml:"vulnerabilityClassifications"`
		Requestresponse              struct {
			Request struct {
				Text   string `xml:",chardata"`
				Method string `xml:"method,attr"`
				Base64 bool   `xml:"base64,attr"`
			} `xml:"request"`
			Response struct {
				Text   string `xml:",chardata"`
				Base64 bool   `xml:"base64,attr"`
			} `xml:"response"`
			ResponseRedirected string `xml:"responseRedirected"`
		} `xml:"requestresponse"`
		IssueDetail string `xml:"issueDetail"`
	} `xml:"issue"`
}

type Testsuites struct {
	XMLName   xml.Name    `xml:"testsuites"`
	Failures  int         `xml:"failures,attr"`
	Name      string      `xml:"name,attr"`
	Tests     int         `xml:"tests,attr"`
	Testsuite []TestSuite `xml:"testsuite"`
}
type TestSuite struct {
	Failures int        `xml:"failures,attr"`
	Name     string     `xml:"name,attr"`
	Tests    int        `xml:"tests,attr"`
	Testcase []TestCase `xml:"testcase"`
}
type TestCase struct {
	Name    string   `xml:"name,attr"`
	Failure *Failure `xml:"failure"`
}
type Failure struct {
	Text    string `xml:",cdata"`
	Message string `xml:"message,attr"`
	Type    string `xml:"type,attr"`
}
func handler(w http.ResponseWriter, r *http.Request) {
    if r.Method != http.MethodPost {
        http.Error(w, "Invalid request method", http.StatusMethodNotAllowed)
        return
    }

    // parse the XML file from the request body
    var data Issues
    if err := xml.NewDecoder(r.Body).Decode(&data); err != nil {
        http.Error(w, "Error parsing XML: "+err.Error(), http.StatusBadRequest)
        return
    }

    // get the severity parameter from the request
    severity := r.URL.Query().Get("severity")
    if severity == "" {
        // if no severity parameter is provided, set it to a default value
        severity = "Information"
    }

    // modify the XML data based on the given severity
    testsuites:=modifyXML(&data, &severity)

    // write the modified XML data to the response
    w.Header().Set("Content-Type", "application/xml")
	w.Write(exportTestsuites(testsuites))
}

func modifyXML(issues *Issues, severity *string)Testsuites {
    // modify the data based on the given severity
	var testsuites Testsuites
	testsuites.Name = "Burpsuite Pro Test"
	testsuites.Tests = len(issues.Issue)
	var tests = make(map[int64]TestSuite)
	for _, issue := range issues.Issue {
		test, found := tests[issue.Type]
		text := fmt.Sprintf(
			template,
			issue.Severity,
			issue.Confidence,
			issue.Host.Text,
			issue.Path,
			issue.IssueDetail,
			issue.IssueBackground,
			issue.RemediationBackground,
			encodeBase64(issue.Requestresponse.Request.Text, issue.Requestresponse.Request.Base64),
			encodeBase64(issue.Requestresponse.Response.Text, issue.Requestresponse.Response.Base64),
			issue.VulnerabilityClassifications)
		testcase := TestCase{Name: issue.Path}
		if isSever(issue.Severity, *severity) {

			testcase.Failure = &Failure{Type: issue.Severity, Text: text, Message: issue.Name + " found at " + issue.Host.Text + issue.Path}
		}
		test.Testcase = append(test.Testcase, testcase)
		if found {

			tests[issue.Type] = test

		} else {
			tests[issue.Type] = TestSuite{Name: issue.Name, Testcase: []TestCase{testcase}}
		}

	}
	for _, item := range tests {
		item.Tests = len(item.Testcase)
		item.Failures = sumFailedTestCases(item.Testcase)
		testsuites.Testsuite = append(testsuites.Testsuite, item)
	}
	testsuites.Failures = sumFailedTotal(testsuites.Testsuite)
	return testsuites

}

func main() {
	inputFile := flag.String("i", "burp-report.xml", "input file")
	outputFile := flag.String("o", "junit-report.xml", "output file")
	severity := flag.String("s", "Information", "severity level")
	server:=flag.Bool("server",false,"Start in server mode")
	port:=flag.Int("port",8888,"Port to listen")
	flag.Parse()
	if *server{
		fmt.Println("Running in server mode")
		fmt.Printf("Listening to *:%d\n",*port)
		http.HandleFunc("/", handler)
		http.ListenAndServe(fmt.Sprintf(":%d",*port), nil)
	}else{
		fileToJunit(severity, inputFile, outputFile)

	}

}

func fileToJunit(severity *string, inputFile *string, outputFile *string) {
	_, valid := severityMap[*severity]
	if !valid {
		fmt.Println("Unknown severity level")
		os.Exit(1)
	}
	xmlFile, err := os.Open(*inputFile)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	defer xmlFile.Close()
	byteValue, _ := ioutil.ReadAll(xmlFile)
	var issues Issues
	xml.Unmarshal(byteValue, &issues)
	testsuites := modifyXML(&issues, severity)
	ioutil.WriteFile(*outputFile, exportTestsuites(testsuites), 0644)
	fmt.Println("Successfully converted burp-report.xml to junit-report.xml")
}

func exportTestsuites(testsuites Testsuites) []byte {
	out, _ := xml.MarshalIndent(testsuites, "", " ")
	out = append([]byte(`<?xml version="1.0" encoding="UTF-8" standalone="no"?>
`), out...)
	return out
}
func isSever(severity, threshold string) bool {
	if val, ok := severityMap[severity]; ok {
		if val <= severityMap[threshold] {
			return false
		}
	}
	return true
}
func sumFailedTestCases(tests []TestCase) int {
	count := 0
	for _, test := range tests {
		if test.Failure != nil {
			if test.Failure.Type != "Information" {
				count++
			}
		}
	}
	return count
}
func sumFailedTotal(tests []TestSuite) int {
	count := 0
	for _, test := range tests {
		count += test.Failures
	}
	return count
}
func encodeBase64(data string, isBase64 bool) string {
	if ! isBase64 {
		output, _ := base64.StdEncoding.DecodeString(data)
		return string(output)
	} else {
		return data
	}
}
