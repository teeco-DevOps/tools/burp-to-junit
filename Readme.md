# Burp to JUnit
This is a simple golang CLI tool that parses BurpSuite's XML report into a JUnit report to be used by compatible readers (e.g. Github Checkrun )
## Usage
This tool can be run in two modes CLI and REST API.
### 1. CLI mode
```sh
go run main.go -i brup-report.xml -o junit-report.xml --severity Low
```
Where burp-report.xml is the BurpSuite XML report and junit-report.xml is the generated JUnit report.

Severity determines which Level of severity should be set as passed in the Junit Report. ( Information,Low,Medium & High. Defaults to Information )

(-i defaults to ./burp-report.xml and -o defaults to ./junit-report.xml)
### 2. REST API
```sh
go run main --server --severity Information --port 8888
curl -X POST http://localhost:8888 -d @brup-report.xml > junit-report.xml
```
The flag --server indicates its running in server mode and --port which defaults to 8888 chooses a port to bind the API.
## Container Image
Comes in two flavors CLI (latest ) and REST API ( server )
```sh
docker run -it -v /path/to/report:/app registry.gitlab.com/teeco-devops/tools/burp-to-junit:latest
docker run -d -p 8888:8888 registry.gitlab.com/teeco-devops/tools/burp-to-junit:server
```