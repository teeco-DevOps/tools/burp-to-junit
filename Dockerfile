FROM golang:1.19-alpine as builder
WORKDIR /go/src/app
COPY . .
RUN CGO_ENABLED=0 go install -ldflags '-extldflags "-static"' -tags timetzdata
FROM scratch
COPY --from=builder /go/bin/burp_to_junit /tojunit
ENTRYPOINT ["/tojunit"]